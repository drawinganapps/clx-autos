import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./views/home-page/home-page.module').then(m => m.HomePageModule)
  },
  {
    path: 'bikes',
    loadChildren: () => import('./views/bike-page/bike-page.module').then(m => m.BikePageModule)
  },
  {
    path: 'bike-details/:id',
    loadChildren: () => import('./views/bike-detail-app/bike-detail-app.module').then(m => m.BikeDetailAppModule)
  },
  {
    path: 'cars',
    loadChildren: () => import('./views/car-page/car-page.module').then(m => m.CarPageModule)
  },
  {
    path: 'car-details/:id',
    loadChildren: () => import('./views/car-detail-page/car-detail-page.module').then(m => m.CarDetailPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
