import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeDetailAppContainerComponent } from './bike-detail-app-container.component';

describe('BikeDetailAppContainerComponent', () => {
  let component: BikeDetailAppContainerComponent;
  let fixture: ComponentFixture<BikeDetailAppContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BikeDetailAppContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BikeDetailAppContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
