import { Component } from '@angular/core';

@Component({
  selector: 'app-bike-detail-app-container',
  templateUrl: './bike-detail-app-container.component.html',
  styleUrls: ['./bike-detail-app-container.component.scss']
})
export class BikeDetailAppContainerComponent {
  constructor() {
  }
}
