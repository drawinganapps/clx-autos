import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BikeDetailAppContainerComponent} from './bike-detail-app-container/bike-detail-app-container.component';
import {RouterModule, Routes} from "@angular/router";
import {HeaderModule} from "../shared/header/header.module";
import {loadRemoteModule} from "@angular-architects/module-federation";
import {ModuleFederationToolsModule} from "@angular-architects/module-federation-tools";

const routes: Routes = [
  {
    path: '',
    component: BikeDetailAppContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: 'http://localhost:5000/remoteEntry.js',
          exposedModule: './BikeDetailModule'
        }).then(m => m.BikeDetailModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    BikeDetailAppContainerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderModule,
    ModuleFederationToolsModule
  ]
})
export class BikeDetailAppModule {
}
