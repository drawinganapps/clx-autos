import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarPageContainerComponent } from './car-page-container.component';

describe('CarPageContainerComponent', () => {
  let component: CarPageContainerComponent;
  let fixture: ComponentFixture<CarPageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarPageContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
