import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarPageContainerComponent} from './car-page-container/car-page-container.component';
import {RouterModule, Routes} from "@angular/router";
import {loadRemoteModule} from "@angular-architects/module-federation";
import {HeaderModule} from "../shared/header/header.module";

const routes: Routes = [
  {
    path: '',
    component: CarPageContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: 'http://localhost:5200/remoteEntry.js',
          exposedModule: './CarListModule'
        }).then(m => m.CarListModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    CarPageContainerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderModule
  ]
})
export class CarPageModule {
}
