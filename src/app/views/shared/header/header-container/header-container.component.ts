import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {get} from "lodash";

enum ItemNavigation {
  BIKES = 'bikes',
  CARS = 'cars'
}

@UntilDestroy()
@Component({
  selector: 'app-header-container',
  templateUrl: './header-container.component.html',
  styleUrls: ['./header-container.component.scss']
})
export class HeaderContainerComponent implements OnInit {

  activatedItem: ItemNavigation = null;
  ItemNavigation: typeof ItemNavigation = ItemNavigation;

  constructor(private route: Router) {
  }

  ngOnInit(): void {
    this.route.events.pipe(
      untilDestroyed(this)
    ).subscribe((route) => {
      const url: string = get(route, 'routerEvent.url') as string;
      console.log(url);
      if (url.toString().includes('bikes')) {
        this.activatedItem = ItemNavigation.BIKES
        return;
      } else if (url.toString().includes('cars')) {
        this.activatedItem = ItemNavigation.CARS;
        return;
      }
      this.activatedItem = null;
    });
  }

  navigate(url: string): void {
    this.route.navigate([`/${url}`])
  }

  isItemActivated(itemMenu: ItemNavigation): string {
    return this.activatedItem === itemMenu ? 'border border-blue-300 rounded-lg p-2' : '';
  }

}
