import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDetailPageContainerComponent } from './car-detail-page-container.component';

describe('CarDetailPageContainerComponent', () => {
  let component: CarDetailPageContainerComponent;
  let fixture: ComponentFixture<CarDetailPageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarDetailPageContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CarDetailPageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
