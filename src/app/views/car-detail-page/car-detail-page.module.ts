import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarDetailPageContainerComponent} from './car-detail-page-container/car-detail-page-container.component';
import {RouterModule, Routes} from "@angular/router";
import {loadRemoteModule} from "@angular-architects/module-federation";
import {HeaderModule} from "../shared/header/header.module";

const route: Routes = [
  {
    path: '',
    component: CarDetailPageContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: 'http://localhost:5200/remoteEntry.js',
          exposedModule: './CarDetailModule'
        }).then(m => m.CarDetailModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    CarDetailPageContainerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    HeaderModule
  ]
})
export class CarDetailPageModule {
}
