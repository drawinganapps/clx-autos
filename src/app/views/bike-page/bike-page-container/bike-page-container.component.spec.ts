import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BikePageContainerComponent } from './bike-page-container.component';

describe('BikePageContainerComponent', () => {
  let component: BikePageContainerComponent;
  let fixture: ComponentFixture<BikePageContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BikePageContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BikePageContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
