import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BikePageContainerComponent} from './bike-page-container/bike-page-container.component';
import {HeaderModule} from "../shared/header/header.module";
import {RouterModule, Routes} from "@angular/router";
import { loadRemoteModule } from '@angular-architects/module-federation';

const routes: Routes = [
  {
    path: '',
    component: BikePageContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => loadRemoteModule({
          type: 'module',
          remoteEntry: 'http://localhost:5000/remoteEntry.js',
          exposedModule: './BikeListModule'
        }).then(m => m.BikeListModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    BikePageContainerComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    RouterModule.forChild(routes)
  ]
})
export class BikePageModule {
}
