import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeContainerComponent} from './home-container/home-container.component';
import {RouterModule, Routes} from "@angular/router";
import {HeaderModule} from "../shared/header/header.module";

const routes: Routes = [
  {
    path: '',
    component: HomeContainerComponent
  }
];

@NgModule({
  declarations: [
    HomeContainerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderModule
  ]
})
export class HomePageModule {
}
